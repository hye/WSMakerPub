import ROOT
import glob, argparse

def guessMassesAndBf(lFiles):
    lMasses = []
    lBfrac  = []
    for file in lFiles:
        print file
        fileName = file.split("/")[-1]
        mass  = int(fileName.split("_")[0])
        bfrac = float(fileName.split("_")[1])
        if not mass in lMasses:
            lMasses.append(mass)
        if not bfrac in lBfrac:
            lBfrac.append(bfrac)
    return lMasses, lBfrac

def getFile(lFiles, mass, bfrac):
    lFilesSelected = [file for file in lFiles if "{}_{}".format(mass, str(bfrac) + "_") in file.split("/")[-1]]
    if len(lFilesSelected) > 1:
        print "Found multiple source files for mass {} and bfrac {}! Use first one!".format(mass, bfrac)
        for file in lFilesSelected: print file
        return lFilesSelected[0]
    if len(lFilesSelected) == 0:
        print "Found no source files for mass {} and bfrac {}!".format(mass, bfrac)
        return None

    return lFilesSelected[0]

def genTable(lFiles, lMasses, lBfrac):
    lMasses.sort()
    lBfrac.sort()

    lTable = []
    for mass in lMasses:
        for bfrac in lBfrac:
            inpFile = getFile(lFiles, mass, bfrac)
            tFile = ROOT.TFile.Open(inpFile, "READ")
            tHist = tFile.Get("limit")
            obs = tHist.GetBinContent(1)
            exp = tHist.GetBinContent(2)
            sig2p = tHist.GetBinContent(3)
            sig1p = tHist.GetBinContent(4)
            sig1m = tHist.GetBinContent(5)
            sig2m = tHist.GetBinContent(6)

            tableLine = "{} {} {} : {} {} {} {} {} {}".format(mass, bfrac, 1-bfrac, obs, exp, sig2p, sig1p, sig1m, sig2m)
            lTable.append(tableLine)
            tFile.Close()
    return lTable

def dumpTable(outDir, name, lTable):
    if not outDir.endswith("/"):
        outDir+="/"

    outFile = open("{}limit-scan-tautau-{}.today.limits-exp-tcr_tt.txt".format(outDir, name), "w")
    for line in lTable:
        outFile.write(line+"\n")
    outFile.close()

def main(args):

    lInpFiles = glob.glob("output/{}*Normal_tcr_tt*/root-files/obs/*.root".format(args.wsDir))
    lMasses, lBfrac = guessMassesAndBf(lInpFiles)
    lTable = genTable(lInpFiles, lMasses, lBfrac)

    dumpTable(args.outputDir, args.name, lTable)

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Tool to dump limit tables for limit plotter")
    parser.add_argument("wsDir", type=str, help="input workspace dir from running WS maker")
    parser.add_argument("--outputDir", type=str, default="./", help="output dir to dump limit table")
    parser.add_argument("--name", type=str, default="test", help="name of production")

    args = parser.parse_args()
    main(args)
