#########################################################################
# File Name: scanLimitByMassAndBF.py
# Description: 
# Author: xzh
# mail: huangxz@ihep.ac.cn
# Created Time: Sat Feb 16 20:11:36 2019
#########################################################################
#!/usr/bin/env python
import os
import subprocess
import time
import ROOT


def create_python_command(workspace,mass, bf):
    """ Create the python command based on the mass point and bf """
    mass = str(mass)
    bf = str(bf)
    command = ["python", "scripts/getLimitByMassAndBF.py"]
    command.append(workspace) # workspace
    command.append("0")  # observed:0 expected:1
    command.append(mass) # mass point
    command.append(bf)   # BF of bbh
    return command

def wait_completion(pids):
    """ Wait until one of the launched jobs is completed """
    while True:
        for pid in pids:
            if pid.poll() is not None:
                print "Process", pid.pid, "has been done."
                pids.remove(pid)
                return
        time.sleep(30)

def wait_completion_all(pids):
    """ Wait until all of the launched jobs are completed """
    while len(pids)>0:
        wait_completion(pids)
    print "All jobs are done !!! Have a nice time !!!"


def submit_local_job(command, log_name):
    """ Submit job executing the specified command, and the output goes to log_name """
    os.system('rm -f ' + log_name)
    out_file = open(log_name, 'w')
    pid = subprocess.Popen(command, stderr=out_file, stdout=out_file)
    print "creating process", pid.pid
    return pid, out_file


if __name__ == "__main__":

    ROOT.gROOT.ProcessLine(".L $WORKDIR/runScanAsymptoticsCLs.C+")

    workspaces= [
            #"200109.Normal_tcr_Htautau_13TeV_Systs"
            "200109.Normal_tcr_tt_Htautau_13TeV_Systs"
            ]

    #masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1500, 2000, 2500] #tautau
    masses = [400,500,600,700] #tautau
    bfs = [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 
           0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0]
    #bfs = [1.0]
    NCORES=20

    """ Do not touch the following statements """
    pids=[]
    logfiles=[]
    for workspace in workspaces:
      for mass in masses:
        for bf in bfs:
            os.system("mkdir -vp output/"+workspace+"_"+str(mass)+"/logs/limit")
            log_name="output/{}/logs/limit/output_{}_{:.2f}.log".format(workspace+"_"+str(mass),mass, bf)
            command = create_python_command(workspace+"_"+str(mass), mass, bf)       
            if len(pids) >= NCORES:
                wait_completion(pids)
            result=submit_local_job(command, log_name)
            pids.append(result[0])
            logfiles.append(result[1])

    wait_completion_all(pids)
    for f in logfiles:
        f.close()
