#!/usr/bin/env python
import os, commands, datetime
from argparse import ArgumentParser
import sys
import numpy as np

doLocal = True

masslist = [200]

ggH_frac_list = np.linspace(0, 1, num=2)
bbH_frac_list = np.linspace(0, 1, num=2)

workspaces = [
    "200126.Normal_tcr_tt_Htautau_13TeV_Systs_",
    ]

for ws in workspaces:
  for mass in masslist:
    for frac_ggH in ggH_frac_list:
      for frac_bbH in bbH_frac_list:
        wsname = ws+str(mass)
        print wsname
        print "mass: %f bbh %f ggh %f total %f" %(mass,frac_bbH,frac_ggH,frac_ggH+frac_bbH)

        if doLocal:
          print "Run locally"
          command = 'source {:s}/scripts/local_NLLScan.sh {:s} {:s} {:s} {:s}'.format(os.curdir,wsname,str(mass), str(frac_ggH), str(frac_bbH))
          print command
          os.system(command)
           
