#!/usr/bin/env python

import sys
import ROOT
import os

if len(sys.argv)<2:
    print( """
Usage:
  python %prog [workspace] [exp/obs] [mass] [frac_bbH]

    expected = 1
    observed = 0
    default mass point = 125
""")
    sys.exit()

ws = sys.argv[1]
if len(sys.argv)>2:
    is_expected = bool(int(sys.argv[2]))
else:
    is_expected = False
if len(sys.argv)>3:
    mass = sys.argv[3]
else:
    mass = "125"
if len(sys.argv)>4:
    frac_bbH = sys.argv[4]
else:
    frac_bbH = "-1"


ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.ProcessLine(".L $WORKDIR/runSig.C+")
ROOT.gSystem.Load('runSig_C.so')

if is_expected:
    suff = "exp"
else:
    suff = "obs"

outdir = "output/"+ws+"/root-files/"+suff+"_p0"
print (outdir)

datname='obsData'
if '@' in mass:
    mass,datname=mass.split('@')

print("Will open workspace", ws)
f = ROOT.TFile.Open("output/{0}/workspaces/combined/{1}.root".format(ws,mass))
w = f.Get("combined")
mc = w.obj("ModelConfig")
pois = mc.GetParametersOfInterest()
print("Number of POIs:", pois.getSize())
it = pois.createIterator()
n = it.Next()
results = {}
while n:
    res = ROOT.runSig("output/"+ws+"/workspaces/combined/"+mass+".root", "combined", "ModelConfig",
                datname, "asimovData_1", "conditionalGlobs_1", "nominalGlobs", n.GetName(),
                mass, outdir, frac_bbH, is_expected)
    results[n.GetName()] = res
    n = it.Next()

outfname = outdir+'/pvalus_'+mass+"_"+frac_bbH+"_"+str( 1-float(frac_bbH))+'.txt'
#outfname = outdir+'/pvalus_'+mass+'.txt'

print("Printing results for all POI:")
for poi, r in results.iteritems():
    print('\n\n')
    print("Results for POI {0}".format(poi))
    print("Observed significance: {0:.6f}".format(r.obs_sig))
    print("Observed pValue: {0:.6f}".format(r.obs_pValue))
    print("Median significance: {0:.6f}".format(r.med_sig))
    print("Median pValue: {0:.6f}".format(r.med_pValue))
    print("Injected significance: {0:.6f}".format(r.inj_sig))
    print("Injected pValue: {0:.6f}".format(r.inj_pValue))
    print('\n\n')
   
    os.system('echo "Results for POI {0}" >> {1}'.format(poi,outfname))
    os.system('echo "Observed significance: {0:.6f}" >> {1}'.format(r.obs_sig,outfname))
    os.system('echo "Observed pValue: {0:.6f}" >> {1}'.format(r.obs_pValue,outfname))
    os.system('echo "Median significance: {0:.6f}" >> {1}'.format(r.med_sig,outfname))
    os.system('echo "Median pValue: {0:.6f}" >> {1}'.format(r.med_pValue,outfname))
    os.system('echo "Injected significance: {0:.6f}" >> {1}'.format(r.inj_sig,outfname))
    os.system('echo "Injected pValue: {0:.6f}" >> {1}'.format(r.inj_pValue,outfname))
