#########################################################################
# File Name: checkBinning.py
# Description: 
# Author: xzh
# mail: huangxz@ihep.ac.cn
# Created Time: Sun 09 Jun 2019 12:32:25 AM CST
#########################################################################
#!/usr/bin/env python
from ROOT import *
from math import *
import array

def addOverflow(hist):
  lastBin = hist.GetNbinsX()

  # content
  lastBinContent = hist.GetBinContent(lastBin)
  overflowBinContent = hist.GetBinContent(lastBin+1)
  if (0 == overflowBinContent): 
    return;
  newContent = lastBinContent+overflowBinContent
  hist.SetBinContent(lastBin, newContent)
  hist.SetBinContent(lastBin+1,0)

  # error
  lastBinError = hist.GetBinError(lastBin)
  overflowBinError = hist.GetBinError(lastBin+1)
  newError = sqrt(lastBinError*lastBinError+overflowBinError*overflowBinError)
  hist.SetBinError(lastBin, newError)
  hist.SetBinError(lastBin+1,0)
  
  # entries
  hist.SetEntries(hist.GetEntries()-2)


def printHist(hist):
  print "{0} has {1} bins, {2} total weights".format(hist.GetName(), hist.GetNbinsX(), hist.GetSumOfWeights())
  #for i in range(1,hist.GetNbinsX()+2):
  #  print hist.GetBinContent(i)

if __name__ == "__main__":

  #input_dir = "inputs/200111_Overflow"
  input_dir = "inputs/200127V1_Overflow"

  region_binning_map = {
    "0tag0jet_0ptv_MuHad":      array.array('d',(50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,2000)),
    "0tag0jet_0ptv_ElHad":      array.array('d',(50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,2000)),
    "1tag0jet_0ptv_MuHad":      array.array('d',(50, 100, 150, 200, 250, 300, 350, 400, 500, 2000)),
    "1tag0jet_0ptv_ElHad":      array.array('d',(50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 2000)),

    "1tag0jet_0ptv_MuHadTcr":   array.array('d',(100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 2000)),
    "1tag0jet_0ptv_ElHadTcr":   array.array('d',(100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 2000)),

    "0tag0jet_0ptv_HadHad":     array.array('d',(150, 200, 250, 300, 350, 400, 450, 500, 600,5000)),
    "1tag0jet_0ptv_HadHad":     array.array('d',(150, 200, 250, 300, 350, 400, 450, 500, 600,5000)),
                        }
  
  sample_list = ["Top","Multijet","Ztautau","Wtaunu","Others","Diboson","DYZ","ZplusJets","WJETSFakes","QCDFakes"]
  print "******************************************************"

  for region, binning in region_binning_map.items():
    input_fn = ""
    if "HadHad" in region:
      input_fn = "{}/13TeV_ZeroLepton_CUT_{}_MTTOT.root".format(input_dir,region)
    else:
      input_fn = "{}/13TeV_OneLepton_CUT_{}_MTTOT.root".format(input_dir,region)
    input_f = TFile.Open(input_fn, "READ") 
    print ("Analyzing file {} ...".format(input_fn))

    ### Obtain the histgrams 
    hist_list = []
    for sample in sample_list:
      hist = input_f.Get(sample)
      if hist :
        hist_list.append(hist)

    ### Add  
    index=0
    for hist in hist_list:
      if 0 == index:
        hist_total = hist.Clone()
        hist_total.SetName("background")
      else:
        hist_total.Add(hist,1.0)
      index = index + 1
      print "Adding histogram {0}".format(hist.GetName())

    ### Print
    addOverflow(hist_total)
    hist_total_rebin = hist_total.Rebin(len(binning)-1, hist_total.GetName()+"_rebin", binning)
    printHist(hist_total_rebin)
    input_f.Close()
