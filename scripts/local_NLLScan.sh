#!/bin/bash

WSNAME="$1"
MASS="$2"
FRACBB="$3"
FRACGG="$4"
CDIR="/scratchfs/atlas/yehf/Stat_BSMtautau/WSMaker_Htautau_official/WSMaker_Htautau"

echo "Running on WS ${WSNAME}"
echo "To get bbH frac ${FRACBB}"
echo "To get ggH frac ${FRACGG}"

echo "Current directory $CDIR :"
python scripts/runFitCrossCheck_frac.py ${WSNAME} 5 ${MASS} ${FRACBB} ${FRACGG}
cat ${CDIR}/output/${WSNAME}/FitCrossChecks_${FRACBB}_${FRACGG}_combined/output_0.log  | grep "FINAL NLL" >> ${CDIR}/GlobalFit_${MASS}.txt
rm -rf ${DIR}/output/${WSNAME}/FitCrossChecks_${FRACBB}_${FRACGG}_combined
