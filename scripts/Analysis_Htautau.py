
#!/usr/bin/env python

import sys
import os
import AnalysisMgr_Htautau as Mgr
import BatchMgr as Batch

#InputVersion = "190530HF2V1"
#InputVersion = "190530HF2V2"
#InputVersion = "190828V1"
#InputVersion = "190901V3_FakeData"
#InputVersion = "190901V3"
#InputVersion = "190919V2"
InputVersion = "070120"

def add_config(mass,  unc = "Systs", one_bin=False,mcstats=True, ireg=1, DoShapePlots=True,DoSystsPlots=True, debug=False,forcebin = 0,var = "MTtot"):
    conf = Mgr.WorkspaceConfig_Htautau(unc, InputVersion=InputVersion, MassPoint=mass, OneBin=one_bin, ForceBinning=forcebin, DoShapePlots=True,DoSystsPlots=True, UseStatSystematics=mcstats, MergeSamples=True, Debug=debug, StatThreshold=0.005,RebinMVAStatUnc=0.05, RebinMVAZsig=10, RebinMVAZbkg=10,RebinMVATrafo=6,DeleteNormFiles=True)
    conf.set_regions(iregions=ireg,var=var)
    fullversion = "Htautau_13TeV_" + unc + "_" + mass
    return conf.write_configs(fullversion)

if __name__ == "__main__":

    if len(sys.argv) is not 2 and len(sys.argv) is not 3 and len(sys.argv) is not 4 and len(sys.argv) is not 5:
        print "Usage: Analysis_Htautau.py <outversion>"
        exit(0)

    outversion = sys.argv[1]
    
    regions = {
        0  : "tcr_tt",
        1  : "tcr_lh",
        2  : "tcr_hh",
        3  : "tcr",
        4  : "lh",
        5  : "hh",
        11 : "lh_bveto",
        12 : "eh_bveto",
        13 : "mh_bveto",
        14 : "tcr_lh_btag",
        15 : "tcr_eh_btag",
        16 : "tcr_mh_btag",
        17 : "hh_bveto",
        18 : "tcr_hh_btag",
    }

    if len(sys.argv) >= 3:
        reginc = int(sys.argv[2])
        outversion += "_" + regions[reginc]
    else:
        reginc = -1
    varx = "MTTOT"
    if len(sys.argv) >= 4:
        varx = str(sys.argv[3])
        outversion += "_" + varx
    if len(sys.argv) >= 5:
        bins = str(sys.argv[4])
    print "outversion: %s" %outversion


    # For running on existing workspaces (-k option):
    #WSlink = "/home/hamity/WorkArea/Limits/WSMaker_for_MSSM-11Jan/xml/20170320-hh-blind.21March_V4_fcc_veto_hh_Htautau_13TeV_21March_V4_fcc_veto_hh_Systs_400/400/400.root" 
    #os.system("ls -lh " + WSlink)

    ########## create config fixles

    print "Adding config files..."
    configfiles_stat = []
    configfiles = []

    zprime = False
    variables = False
    masses = [200, 250, 300, 350, 400, 500, 600, 700, 800, 1000, 1200, 1500, 2000, 2500]
    #masses = [600]
    if zprime:
        masses = [ 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 2750, 3000, 3250, 3500, 3750, 4000 ] #zprime
    zforcebin = [ " 400, 5000" , "450,5000", "500,5000", "550,5000", "650,5000", "750,5000"] #zprime
    for m in masses:
        if zprime:
            if m < 750:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[0], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[0], DoShapePlots=True, DoSystsPlots=True, debug=True)
            elif m < 850:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[1], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[1], DoShapePlots=True, DoSystsPlots=True, debug=True)
            elif m < 950:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[2], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[2], DoShapePlots=True, DoSystsPlots=True, debug=True)
            elif m < 1050:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[3], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[3], DoShapePlots=True, DoSystsPlots=True, debug=True)
            elif m < 1450:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[4], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[4], DoShapePlots=True, DoSystsPlots=True, debug=True)
            else:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[5], DoShapePlots=True, DoSystsPlots=True, debug=True)
                    configfiles_stat      += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, forcebin = zforcebin[5], DoShapePlots=True, DoSystsPlots=True, debug=True)
        elif variables:
            configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, forcebin = bins, DoShapePlots=True, DoSystsPlots=True, debug=True,var=varx)

        else:
                    configfiles      += add_config(str(m), "Systs", one_bin=False, mcstats=True, ireg=reginc, DoShapePlots=True, DoSystsPlots=True, debug=True, var=varx)
                    configfiles_stat += add_config(str(m), "StatOnly", one_bin=False, mcstats=True, ireg=reginc, debug=True, var=varx)
            
        
    for fi in configfiles_stat:
        print fi
    for fi in configfiles:
        print fi

    #sys.exit()

    ########## define tasks

    tasks = ["-w"]             # create workspace (do always)
    try:
        tasks += ["-k", WSlink]    # link workspace instead of creating one (requires -w)
    except NameError:
        pass
    #  tasks += ["-l", "0,{MassPoint}"]         # limit (1 expected, 0 obs)
#     tasks += ["-s", 0]         # significance
#    tasks += ["--fcc", "4,8@{MassPoint}"]  # fitCrossChecks
#    tasks += ["--fcc", "2@{MassPoint}"]  # fitCrossChecks
#    tasks += ["--fcc", "2,4,8,10@{MassPoint}"]  # fitCrossChecks
#    tasks += ["-r", "{MassPoint}"]         # ranking
#    tasks += ["-n", "{MassPoint}"]         # ranking                                                                                                             

#     tasks += ["-u", 0]         # breakdown of muhat errors stat/syst/theory
#     # requires fitCrossChecks:
#     tasks += ["-m", "7,2"]     # reduced diag plots (quick)
#     tasks += ["-b", "2"]       # unfolded b-tag plots (quick)
    #tasks += ["-p", "2,8@{MassPoint}"]       # post-fit plots (CPU intense)
#     tasks += ["-t", "0,2"]     # yield tables (CPU intense)
    
    ########## submit jobs

    # in case you want to run something locally
    #Batch.run_local_batch(configfiles_stat, outversion, tasks)
    Batch.run_local_batch(configfiles, outversion, tasks)
    sys.exit()
    #Batch.run_local_batch(configfiles_stat, outversion, ["-w", "-l", 1])

    # adjust to recover failed ranking subjobs
    redoSubJob = -1

    if redoSubJob < 0:
        print "Submitting stat only jobs..."
        Batch.run_lxplus_batch(configfiles_stat, outversion, ["-w", "-l", 1,"-s",1], '2nd')
        print "Submitting systs jobs..."
        Batch.run_lxplus_batch(configfiles, outversion, tasks, '2nd')
    print "Submitting rankings..."
    Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-r", "125"], '2nd', jobs=20, subJob=redoSubJob)
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "125"], '2nd', jobs=20, subJob=redoSubJob)

    ########## non-default stuff. Warning: don't submit rankings / toys / NLL scans with the same outversion and nJobs!
    #print "Submitting toys..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "6"], '2nd', jobs=50, subJob=redoSubJob)
    #print "Submitting NLL scans..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "7,2,doNLLscan"], '2nd', jobs=50, subJob=redoSubJob)
