#########################################################################
# File Name: scanLimitByMassAndBF.py
# Description: 
# Author: xzh
# mail: huangxz@ihep.ac.cn
# Created Time: Sat Feb 16 20:11:36 2019
#########################################################################
#!/usr/bin/env python
import os
import subprocess
import time
import ROOT


def create_python_command(workspace,mass, bf,NPname):
    """ Create the python command based on the mass point and bf """
    mass = str(mass)
    bf = str(bf)
    NPname = str(NPname)
    command = ["python", "scripts/getLimitByMassAndBF_NP.py"]
    command.append(workspace) # workspace
    command.append("1")  # observed:0 expected:1
    command.append(mass) # mass point
    command.append(bf)   # BF of bbh
    command.append(NPname)   # name of NP
    return command

def wait_completion(pids):
    """ Wait until one of the launched jobs is completed """
    while True:
        for pid in pids:
            if pid.poll() is not None:
                print "Process", pid.pid, "has been done."
                pids.remove(pid)
                return
        time.sleep(30)

def wait_completion_all(pids):
    """ Wait until all of the launched jobs are completed """
    while len(pids)>0:
        wait_completion(pids)
    print "All jobs are done !!! Have a nice time !!!"


def submit_local_job(command, log_name):
    """ Submit job executing the specified command, and the output goes to log_name """
    os.system('rm -f ' + log_name)
    out_file = open(log_name, 'w')
    pid = subprocess.Popen(command, stderr=out_file, stdout=out_file)
    print "creating process", pid.pid
    return pid, out_file


if __name__ == "__main__":

    #ROOT.gROOT.ProcessLine(".L $WORKDIR/runScanAsymptoticsCLs_removeNP.C+")
    ROOT.gROOT.ProcessLine(".L $WORKDIR/runScanAsymptoticsCLs_NP.C+")

    #workspace="200126.StatOnly_tcr_tt_Htautau_13TeV_Systs"
    #workspace="200126.Fake_tcr_tt_Htautau_13TeV_Systs"
    #workspace="200126.TauES_tcr_tt_Htautau_13TeV_Systs"
    #workspace="200126.TauEff_tcr_tt_Htautau_13TeV_Systs"
    #workspace="200126.LPX_tcr_tt_Htautau_13TeV_Systs"
    workspace="200126.Others_tcr_tt_Htautau_13TeV_Systs"

    masses = [200, 300, 400, 600, 1000, 1500, 2000, 2500] #tautau
    bfs = [0.0,1.0]
    #NPnames = ["StatOnly"]
    #NPnames = ["Fake"]
    #NPnames = ["TauES"]
    #NPnames = ["TauEff"]
    #NPnames = ["LPX"]
    NPnames = ["Others"]
    NCORES=10

    """ Do not touch the following statements """
    pids=[]
    logfiles=[]
    for mass in masses:
        for bf in bfs:
            for NPname in NPnames:
                os.system("mkdir -vp output/"+workspace+"_"+str(mass)+"/logs/limit")
                log_name="output/{0}/logs/limit/output_{1}_{2}_{3}.log".format(workspace+"_"+str(mass),mass, bf, NPname)
                command = create_python_command(workspace+"_"+str(mass), mass, bf, NPname)       
                if len(pids) >= NCORES:
                    wait_completion(pids)
                result=submit_local_job(command, log_name)
                pids.append(result[0])
                logfiles.append(result[1])

    wait_completion_all(pids)
    for f in logfiles:
        f.close()
