#########################################################################
# File Name: analysis.py
# Description: 
# Author: xzh
# mail: huangxz@ihep.ac.cn
# Created Time: Wed 11 Sep 2019 05:48:37 PM CST
#########################################################################
#!/usr/bin/env python

import ROOT
import os
from math import *

#add overflow to the last bin
def add_overflow(hist):
  lastBin = hist.GetNbinsX()
  # value
  lastBinContent = hist.GetBinContent(lastBin)
  overflowBinContent = hist.GetBinContent(lastBin+1)
  if (overflowBinContent==0.0):
    return;
  #else:
  #  print ("Add the overflow of {}, {}".format(hist.GetName(), overflowBinContent))
  newContent = lastBinContent+overflowBinContent
  hist.SetBinContent(lastBin, newContent)
  hist.SetBinContent(lastBin+1,0)
  # error
  lastBinError = hist.GetBinError(lastBin)
  overflowBinError = hist.GetBinError(lastBin+1)
  newError = sqrt(lastBinError*lastBinError+overflowBinError*overflowBinError)
  hist.SetBinError(lastBin, newError)
  hist.SetBinError(lastBin+1,0)
  # entries
  hist.SetEntries(hist.GetEntries()-2)


def ana_hist(in_dir, hist_name, out_dir):
  hist = in_dir.Get(hist_name)
  new_hist = hist.Clone()
  add_overflow(new_hist)
  out_dir.WriteTObject(new_hist)

def main(input_fn, output_fn):
  input_f = ROOT.TFile(input_fn, 'r')
  output_f = ROOT.TFile(output_fn, 'recreate')
  
  print ("Analyze file: ", input_fn)
  keys = input_f.GetListOfKeys()
  for key in keys:
    if key.GetClassName()  == 'TH1F':
      ana_hist(input_f, key.GetName(), output_f)
    elif key.GetClassName() == 'TDirectoryFile':
      out_dir = output_f.mkdir(key.GetName())
      in_dir = input_f.Get(key.GetName())
      dir_keys = in_dir.GetListOfKeys()
      for dir_key in dir_keys:
        ana_hist(in_dir, dir_key.GetName(), out_dir)

  input_f.Close()
  output_f.Close()

if __name__ == '__main__': 

  input_dir = "./inputs/200127V1"
  output_dir = input_dir + "_Overflow"
    
  os.system("mkdir -p {}".format(output_dir))

  file_list = [
      "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_MTTOT.root",
      "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_MTTOT.root",
      "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_MTTOT.root",
      "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_MTTOT.root",
      "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_MTTOT.root",
      "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_MTTOT.root",
      "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_MTTOT.root",
      "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_MTTOT.root",
          ]

  for afile in file_list:
    # Analysis the file
    main(input_dir+'/'+afile, output_dir+'/'+afile)
