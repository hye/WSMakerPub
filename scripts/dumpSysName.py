#!/usr/bin/env python
import ROOT


def readDir(fileName):
  f=ROOT.TFile(fileName, "read") 
  keys=f.GetListOfKeys() 
  dirs={}
  for key in keys:
    className=key.GetClassName() 
    if (className=="TDirectoryFile"):
      dirName=key.GetName()
      dirName=dirName.replace("__1up","")
      dirName=dirName.replace("__1down","")
      dirName=dirName.replace("__high","")
      dirName=dirName.replace("__low","")
    
      if dirName in dirs.keys():
        dirs[dirName] += 1
      else:
        dirs[dirName] = 1
  return dirs


if __name__ == '__main__':
  #fileName="inputs/20190606/13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_MTTOT.root"
  #fileName="inputs/20190707/13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_MTTOT.root"
  fileName="inputs/190530HF2TmpBugFix/13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_MTTOT.root"
  #fileName="inputs/190530HF2TmpBugFix/13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_MTTOT.root"
  dirs=readDir(fileName)

  print ("There are {0} sys in total.".format(len(dirs))) 
  items = dirs.items()
  items.sort()
  for key, value in items:
    print ('"{0}", {1}'.format(key, value))


