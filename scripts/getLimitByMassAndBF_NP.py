#!/usr/bin/env python

import sys
import ROOT
import os

if len(sys.argv)<3:
    print( """
Usage: 
  python %prog [workspace] [exp/obs] [mass] [frac_bbH] [NPname]

    expected = 1
    observed = 0
    default mass point = 125
    default frac_bbH = -1
    default NPname = "StatOnly"
""")
    sys.exit()

ws = sys.argv[1]
NPname = sys.argv[5]


if len(sys.argv)>2:
    is_expected = bool(int(sys.argv[2]))
else:
    is_expected = False
if len(sys.argv)>3:
    mass = sys.argv[3]
else:
    mass = "125"
if len(sys.argv)>4:
    frac_bbH = sys.argv[4]
else:
    frac_bbH = "-1"
if len(sys.argv)>5:
    NPname = sys.argv[5]
else:
    NPname = "StatOnly"

ROOT.gROOT.SetBatch(True)
#ROOT.gROOT.ProcessLine(".L $WORKDIR/runScanAsymptoticsCLs.C+")
#ROOT.gSystem.Load('runScanAsymptoticsCLs_removeNP_C.so')
ROOT.gSystem.Load('runScanAsymptoticsCLs_NP_C.so')

if is_expected:
    ROOT.doExpected(True)
    suff = "exp"
else:
    ROOT.doExpected(False)
    suff = "obs"

#FIXME should be an option
#ROOT.doBetterBands(False)
ROOT.doBetterBands(True)

ROOT.doInjection(False)

outdir = "output/"+ws+"/root-files/"+suff;

ROOT.runAsymptoticsCLs_NP("output/"+ws+"/workspaces/combined/"+mass+".root", "combined", "ModelConfig", "obsData", "", outdir, mass, 0.95, frac_bbH, NPname)


f=ROOT.TFile(outdir+"/"+mass+"_"+frac_bbH+"_"+str( 1-float(frac_bbH))+".root")
lim = f.Get("limit")
med = lim.GetBinContent(2)
p2 = lim.GetBinContent(3)
p1 = lim.GetBinContent(4)
m1 = lim.GetBinContent(5)
m2 = lim.GetBinContent(6)
obs = lim.GetBinContent(1)
inj = lim.GetBinContent(7)

print( "Injected limit:", inj)
print( "Expected limit:", med, "+", p1-med, "-", med-m1)
print( "Observed limit:", obs)
print( "{0:.2f}".format(med))

outfname = outdir+'/limit_'+mass+"_"+frac_bbH+"_"+NPname+"_"+str( 1-float(frac_bbH))+'.txt'
os.system('echo "Injected limit: '+str(inj)+'" > '+ outfname)
os.system('echo "Expected limit: '+str(med)+' +'+str(p1-med)+' -'+str(med-m1)+'" >> '+ outfname)
os.system('echo "Observed limit: '+str(obs)+'" >> '+outfname)
os.system('echo "Expected limit: {0:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}" >> {3}'.format(med, p1-med, med-m1, outfname))
os.system('echo "Observed limit: {0:.2f}" >> {1}'.format(obs, outfname))
os.system('echo " " >> {0}'.format(outfname))
os.system('echo "obs -2s -1s exp +1s _2s" >> {0}'.format(outfname))
os.system('echo "{0:.5f} & {1:.5f} &  {2:.5f} & {3:.5f} & {4:.5f} & {5:.5f} \\\\" >> {6}'.format(obs, m2, m1, med, p1, p2, outfname))

#print( "Now adding asimov data with mu at expected limit.")
#ROOT.RooWorkspace.rfimport = getattr(ROOT.RooWorkspace, 'import')
#
#f = ROOT.TFile("output/"+ws+"/workspaces/combined/"+mass+".root")
#wsm = f.Get("combined")
#
#print( "Data contained in the original workspace: ")
#allData = wsm.allData()
#alreadyExists = False
#while allData.size() > 0:
#    data = allData.front()
#    allData.pop_front()
#    print( "   ==>",data.GetName())
#    if data.GetName() == "asimovDataAtLimit":
#        alreadyExists = True
#
#if alreadyExists:
#    print( "asimovDataAtLimit already exists")
#else:
#    data = wsm.data("obsData")
#
#    mc = wsm.obj("ModelConfig")
#
#    poi = mc.GetParametersOfInterest().first();
#
#    poi.setVal(med);
#
#    allParams = mc.GetPdf().getParameters(data);
#    ROOT.RooStats.RemoveConstantParameters(allParams)
#
#    globObs = ROOT.RooArgSet("globObs")
#    asimov_data = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(mc, allParams, globObs)
#
#    asimov_data.SetName("asimovDataAtLimit")
#    wsm.rfimport(asimov_data)
#
#    print( "Data contained in the modified workspace: ")
#    allData = wsm.allData()
#    while allData.size() > 0:
#        data = allData.front()
#        allData.pop_front()
#        print( "   ==>",data.GetName())
#    
#    wsm.writeToFile("output/"+ws+"/workspaces/combined/"+mass+".root")


