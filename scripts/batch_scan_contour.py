import os, commands, datetime
from argparse import ArgumentParser
from subprocess import call
import sys
import numpy as np

masslist = [1200]
### obs ###
#200 0.4
#250 0.3
#300 0.3
#350 0.3
#400 0.1
#500 0.1
#600 0.02
#700 0.01
#800 0.005
#1000 0.005
#1200 0.005
#1500 0.002
#2000 0.002
#2500 0.002

### exp ###
#200 0.4

#ggH_frac_list = [0,1]
#bbH_frac_list = [0,1]
ggH_frac_list = np.linspace(0, 0.005, num=100)
bbH_frac_list = np.linspace(0, 0.005, num=100)



### conditional fit to observed data: 5
### conditional fit to Asimov data: 9
tmp = """#!/bin/sh
WSNAME="%s"
MASS="%s"
FRACBB="%s"
FRACGG="%s"
NUMBER="%s"

DIR="/scratchfs/atlas/yehf/Stat_BSMtautau/WSMaker_Htautau_official/WSMaker_Htautau"

#cd /tmp
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
cd ${DIR}
source ${DIR}/setup.sh

echo "Running on WS ${WSNAME}"
echo "To get bbH frac ${FRACBB}"
echo "To get ggH frac ${FRACGG}"

python ${DIR}/scripts/runFitCrossCheck_frac.py ${WSNAME} 5 ${MASS} ${FRACBB} ${FRACGG}
cat ${DIR}/output/${WSNAME}/FitCrossChecks_${FRACBB}_${FRACGG}_combined/output_0.log  | grep "FINAL NLL" >> ${DIR}/GlobalFit_${MASS}_v2.txt
rm -rf ${DIR}/output/${WSNAME}/FitCrossChecks_${FRACBB}_${FRACGG}_combined
rm ${DIR}/scripts/checksubmissionTemp/${WSNAME}_${NUMBER}.sh
"""

workspaces = [
              "200126.Normal_tcr_tt_Htautau_13TeV_Systs_",
             ]

num = 0

for ws in workspaces:
  for mass in masslist:
    for frac_ggH in ggH_frac_list:
      for frac_bbH in bbH_frac_list:
        wsname = ws+str(mass)
        #print wsname
        print "mass: %f bbh %f ggh %f total %f" %(mass,frac_bbH,frac_ggH,frac_ggH+frac_bbH)

        bsubscript = tmp % (wsname,str(mass),str(frac_bbH),str(frac_ggH),str(num)) + '\n'
        #exc = open('./scripts/submissionWorkloadTemp/'+wsname+'_'+str(frac_bbH)+'_'+str(frac_ggH)+'.sh', 'w')
        exc = open('./scripts/submissionWorkloadTemp/'+wsname+'_'+str(num)+'.sh', 'w')
        exc.write(bsubscript)
        exc.close()
        #call(['chmod', '744', './scripts/submissionWorkloadTemp/'+wsname+'_'+str(frac_bbH)+'_'+str(frac_ggH)+'.sh']) 
        call(['chmod', '744', './scripts/submissionWorkloadTemp/'+wsname+'_'+str(num)+'.sh']) 
        num = num + 1
