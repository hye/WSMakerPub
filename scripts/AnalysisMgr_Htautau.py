from AnalysisMgr import WorkspaceConfig

class WorkspaceConfig_Htautau(WorkspaceConfig):
    def __init__(self, *args, **kwargs):
        kwargs["Analysis"] = "Htautau"
        super(WorkspaceConfig_Htautau, self).__init__(*args, **kwargs)
        return

    def set_regions(self, iregions=0, var = "MTtot"):

        if iregions==0:
            ## Default fit: tcr_tt 
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_"+var,
                "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_"+var
                ]
        elif iregions==1:
            ## tcr_lh 
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                ]
        elif iregions==2:
            ## tcr_hh
            self["Regions"] = [
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_"+var,
                "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_"+var
                ]
        elif iregions==3:
            ## tcr
            self["Regions"] = [
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                ]
        elif iregions==4:
            ## lh
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                ]
        elif iregions==5:
            ## hh
            self["Regions"] = [
                "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_"+var,
                "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_"+var
                ]
        elif iregions==11:
            ## lh bveto
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                ]
        elif iregions==12:
            ## ehad bveto
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                ]
        elif iregions==13:
            ## muhad bveto
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                ]
        elif iregions==14:
            ## tcr lh btag
            self["Regions"] = [
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                ]
        elif iregions==15:
            ## tcr ehad btag
            self["Regions"] = [
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                ]
        elif iregions==16:
            ## tcr muhad btag
            self["Regions"] = [
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                ]
        elif iregions==17:
            ## hadhad bveto
            self["Regions"] = [
                "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_"+var,
                ]
        elif iregions==18:
            ## tcr hadhad btag
            self["Regions"] = [
                "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHadTcr_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHadTcr_"+var,
                ]
        else :
            ## tt 
            self["Regions"] = [
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_0tag0jet_0ptv_MuHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_ElHad_"+var,
                "13TeV_OneLepton_CUT_1tag0jet_0ptv_MuHad_"+var,
                "13TeV_ZeroLepton_CUT_0tag0jet_0ptv_HadHad_"+var,
                "13TeV_ZeroLepton_CUT_1tag0jet_0ptv_HadHad_"+var
            ]

        self.check_regions()
