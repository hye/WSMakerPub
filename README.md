# WSMaker for H/A to $\tau\tau$ analysis

This repository is meant to construct the statistical analysis for the BSM $H/A\rightarrow\tau\tau$ analysis using the [WSMaker](https://gitlab.cern.ch/atlas-phys-hdbs-htautau/WSMaker/tree/Htautau_dev) (commit id: ) framework.

## Setup

1. [Fork](https://gitlab.cern.ch/atlas-phys-hdbs-htautau/WSMaker_Htautau/-/forks/new) the project. 

2. Clone the project.

   ```
   ### set up the environment
   setupATLAS
   lsetup git
   
   ### go to the workarea
   mkdir myWorkDir
   cd myWorkDir
   
   ### clone
   ### There are a few different protocal options for cloning the project.
   git clone --recursive ssh://git@gitlab.cern.ch:7999/${USER}/WSMaker_Htautau.git
   #git clone --recursive https://gitlab.cern.ch/${USER}/WSMaker_Htautau.git
   #git clone --recursive https://:@gitlab.cern.ch:8443/${USER}/WSMaker_Htautau.git
   
   ### add the upstream
   cd WSMaker_Htautau 
   git remote add upstream ssh://git@gitlab.cern.ch:7999/atlas-phys-hdbs-htautau/WSMaker_Htautau.git
   #git remote add upstream https://gitlab.cern.ch/atlas-phys-hdbs-htautau/WSMaker_Htautau.git
   #git remote add upstream https://:@gitlab.cern.ch:8443/atlas-phys-hdbs-htautau/WSMaker_Htautau.git
   
   ### checkout the specific branch
   ###   developed for full run2 analysis
   git checkout 190828V1 
   ###   developed for reproducing the results of previous paper
   #git checkout 20170710-mssm
   git submodule update
   ```

3. build the project

   ```bash
   ### setup the environment
   source setup.sh

   ### compile 
   cd build && cmake ..
   make -j5
   ```

   

## On Every Login

```bash
source setup.sh
```



## Runing the Statistical Analysis

### Input 

The input histograms for WSMaker can be found in my eos `/afs/cern.ch/user/x/xiaozhon/eos/BSMHtautau/Inputs` 
Please copy them into the `inputs` directory.


### Creation of the workspace

```bash
python scripts/Analysis_Htautau.py VERSION CHANNEL
```

`VERSION` could be any string. It is to remind you the configuration of the workspace

`CHANNEL` should be an integer. It is defined in `scripts/AnalysisMgr_Htautau.py`



### Obtain the upper limit

```bash
python scripts/scanLimitByMassAndBF.py
```

### Calculate significance

```bash
python scripts/scanSigByMass.py
```

### run 2D NLL scanning

Run locally

```bash
python scripts/local_scan_contour.py
```

Get submission files to run on batch system
```bash
python scripts/batch_scan_contour.py
```

### add individual systematic uncertainty to check systematics impact

```bash
python scripts/scanLimitByMassAndBF_NP.py
```



## Notes for the branch `20170710-mssm`

There are several bugs in the old WSMaker project.

- systematic uncertainty of signal acceptance for ggh fusion not applied to $\tau_h\tau_h$ signal samples
- systematic uncertainty of `LUMI` not applied to signal samples
- a bug related to the smooth of systematic uncertainties

To reproduce the results of previous paper, all of the listed bugs are not fixed in the branch `20170710-mssm`. 

