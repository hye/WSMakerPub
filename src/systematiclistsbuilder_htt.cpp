
#include "systematiclistsbuilder_htt.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <stdlib.h>     /* atof */
#include <iostream>

#include <TString.h>
#include <TGraph.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/properties.hpp"
#include "WSMaker/finder.hpp"
#include "WSMaker/sample.hpp"
#include "WSMaker/systematic.hpp"

void SystematicListsBuilder_Htautau::fillHistoSystsRenaming() {
  /// LPX Kfactor 12
  m_renameHistoSysts["LPX_KFACTOR_ALPHAS_Corr"] = "ATLAS_LPX_KFACTOR_ALPHAS_Corr";
  m_renameHistoSysts["LPX_KFACTOR_BEAM_ENERGY_Corr"] = "ATLAS_LPX_KFACTOR_BEAM_ENERGY_Corr";
  m_renameHistoSysts["LPX_KFACTOR_CHOICE_HERAPDF20_Corr"] = "ATLAS_LPX_KFACTOR_CHOICE_HERAPDF20_Corr";
  m_renameHistoSysts["LPX_KFACTOR_CHOICE_NNPDF30_Corr"] = "ATLAS_LPX_KFACTOR_CHOICE_NNPDF30_Corr";
  m_renameHistoSysts["LPX_KFACTOR_CHOICE_epWZ16_Corr"] = "ATLAS_LPX_KFACTOR_CHOICE_epWZ16_Corr";
  m_renameHistoSysts["LPX_KFACTOR_PDF_Corr"] = "ATLAS_LPX_KFACTOR_PDF_Corr";
  m_renameHistoSysts["LPX_KFACTOR_PDF_EW_Corr"] = "ATLAS_LPX_KFACTOR_PDF_EW_Corr";
  m_renameHistoSysts["LPX_KFACTOR_PDF_epWZ16_Corr"] = "ATLAS_LPX_KFACTOR_PDF_epWZ16_Corr";
  m_renameHistoSysts["LPX_KFACTOR_PI_Corr"] = "ATLAS_LPX_KFACTOR_PI_Corr";
  m_renameHistoSysts["LPX_KFACTOR_REDCHOICE_NNPDF30_Corr"] = "ATLAS_LPX_KFACTOR_REDCHOICE_NNPDF30_Corr";
  m_renameHistoSysts["LPX_KFACTOR_SCALE_W_Corr"] = "ATLAS_LPX_KFACTOR_SCALE_W_Corr";
  m_renameHistoSysts["LPX_KFACTOR_SCALE_Z_Corr"] = "ATLAS_LPX_KFACTOR_SCALE_Z_Corr";           

  /// TTBAR 4
  m_renameHistoSysts["TTBAR_Radiation"] = "ATLAS_TTBAR_Radiation";
  m_renameHistoSysts["TTBAR_ShowerUE"] = "ATLAS_TTBAR_ShowerUE";
  m_renameHistoSysts["TTBAR_FSR"] = "ATLAS_TTBar_FSR";
  m_renameHistoSysts["TTBAR_ISR"] = "ATLAS_TTBar_ISR";
  m_renameHistoSysts["TTBAR_ME"] = "ATLAS_TTBar_ME";
  m_renameHistoSysts["TTBAR_PS"] = "ATLAS_TTBar_PS";

  /// JET 17 
  m_renameHistoSysts["JET_EtaIntercalibration_NonClosure_highE__continuous"] = "ATLAS_JET_EtaIntercalibration_NonClosure_highE";
  m_renameHistoSysts["JET_EtaIntercalibration_NonClosure_negEta__continuous"] = "ATLAS_JET_EtaIntercalibration_NonClosure_negEta";
  m_renameHistoSysts["JET_EtaIntercalibration_NonClosure_posEta__continuous"] = "ATLAS_JET_EtaIntercalibration_NonClosure_posEta";
  m_renameHistoSysts["JET_Flavor_Response__continuous"] = "ATLAS_JET_Flavor_Response";
  m_renameHistoSysts["JET_GroupedNP_1__continuous"] = "ATLAS_JET_GroupedNP_1";
  m_renameHistoSysts["JET_GroupedNP_2__continuous"] = "ATLAS_JET_GroupedNP_2";
  m_renameHistoSysts["JET_GroupedNP_3__continuous"] = "ATLAS_JET_GroupedNP_3";
  m_renameHistoSysts["JET_JER_DataVsMC_MC16__continuous"] = "ATLAS_JET_JER_DataVsMC_MC16";
  m_renameHistoSysts["JET_JER_EffectiveNP_1__continuous"] = "ATLAS_JET_JER_EffectiveNP_1";
  m_renameHistoSysts["JET_JER_EffectiveNP_2__continuous"] = "ATLAS_JET_JER_EffectiveNP_2";
  m_renameHistoSysts["JET_JER_EffectiveNP_3__continuous"] = "ATLAS_JET_JER_EffectiveNP_3";
  m_renameHistoSysts["JET_JER_EffectiveNP_4__continuous"] = "ATLAS_JET_JER_EffectiveNP_4";
  m_renameHistoSysts["JET_JER_EffectiveNP_5__continuous"] = "ATLAS_JET_JER_EffectiveNP_5";
  m_renameHistoSysts["JET_JER_EffectiveNP_6__continuous"] = "ATLAS_JET_JER_EffectiveNP_6";
  m_renameHistoSysts["JET_JER_EffectiveNP_7restTerm__continuous"] = "ATLAS_JET_JER_EffectiveNP_7restTerm";
  m_renameHistoSysts["JET_TILECORR_Uncertainty"] = "ATLAS_JET_TILECORR_Uncertainty";
  m_renameHistoSysts["JET_JvtEfficiency_Corr"] = "ATLAS_jet_jvteff";

  /// btag: 14
  m_renameHistoSysts["FT_EFF_Eigen_B_0_Corr"] = "ATLAS_btag_b_0";
  m_renameHistoSysts["FT_EFF_Eigen_B_1_Corr"] = "ATLAS_btag_b_1";
  m_renameHistoSysts["FT_EFF_Eigen_B_2_Corr"] = "ATLAS_btag_b_2";
  m_renameHistoSysts["FT_EFF_Eigen_C_0_Corr"] = "ATLAS_btag_c_0";
  m_renameHistoSysts["FT_EFF_Eigen_C_1_Corr"] = "ATLAS_btag_c_1";
  m_renameHistoSysts["FT_EFF_Eigen_C_2_Corr"] = "ATLAS_btag_c_2";
  m_renameHistoSysts["FT_EFF_Eigen_C_3_Corr"] = "ATLAS_btag_c_3";
  m_renameHistoSysts["FT_EFF_Eigen_Light_0_Corr"] = "ATLAS_btag_light_0";
  m_renameHistoSysts["FT_EFF_Eigen_Light_1_Corr"] = "ATLAS_btag_light_1";
  m_renameHistoSysts["FT_EFF_Eigen_Light_2_Corr"] = "ATLAS_btag_light_2";
  m_renameHistoSysts["FT_EFF_Eigen_Light_3_Corr"] = "ATLAS_btag_light_3";
  m_renameHistoSysts["FT_EFF_Eigen_Light_4_Corr"] = "ATLAS_btag_light_4";
  m_renameHistoSysts["FT_EFF_extrapolation_Corr"] = "ATLAS_btag_extrapolation";
  m_renameHistoSysts["FT_EFF_extrapolation_from_charm_Corr"] = "ATLAS_btag_extrapolation_from_charm";

  /// tau rec, id, res, scale: 7
  m_renameHistoSysts["TAUS_TRUEHADTAU_EFF_RECO_HIGHPT"] = "ATLAS_tau_eff_reco_highpt";
  m_renameHistoSysts["TAUS_TRUEHADTAU_EFF_RECO_TOTAL"] = "ATLAS_tau_eff_reco_total";
  m_renameHistoSysts["TAUS_TRUEHADTAU_SME_TES_DETECTOR"] = "ATLAS_TAUS_TRUEHADTAU_SME_TES_DETECTOR";
  m_renameHistoSysts["TAUS_TRUEHADTAU_SME_TES_INSITU"] = "ATLAS_TAUS_TRUEHADTAU_SME_TES_INSITU";
  m_renameHistoSysts["TAUS_TRUEHADTAU_SME_TES_MODEL"] = "ATLAS_TAUS_TRUEHADTAU_SME_TES_MODEL";
  m_renameHistoSysts["TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__LTAU_MED"] = "ATLAS_tau_eff_jetid_highpt";
  m_renameHistoSysts["TAUS_TRUEHADTAU_EFF_JETID_SYST__LTAU_MED"] = "ATLAS_tau_eff_jetid_total";

  // MET: 3
  m_renameHistoSysts["MET_SoftTrk_ResoPara"] = "ATLAS_MET_SoftTrk_ResoPara";
  m_renameHistoSysts["MET_SoftTrk_ResoPerp"] = "ATLAS_MET_SoftTrk_ResoPerp";
  m_renameHistoSysts["MET_SoftTrk_Scale"] = "ATLAS_MET_SoftTrk_Scale";
  
  // pileup: 1
  m_renameHistoSysts["PRW_DATASF_Corr"] = "ATLAS_pu_prw";
}

void SystematicListsBuilder_Htautau::listAllUserSystematics(bool useFltNorms) {

  /// add POIs
  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");
  if(sig_decorr.size() != 0) { // TODO or just extend decorrSysForCategories for the POIs ?
    std::vector<Property> decorrelations;
    for(auto& d : sig_decorr) {
      decorrelations.push_back(Properties::props_from_names.at(d));
    }
    addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 0, -40, 40);
  }
  else {
    addPOI("SigXsecOverSM", {"Sig"}, 0, -40, 40);
  }

  /// Norm factor: bfs
  TString massPoint = m_config.getValue("MassPoint", "125");

#if 1
  normFact( "Norm_ggH", {"ggHlh0tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHlh0tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHlh1tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHlh1tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHhh0tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHhh0tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHhh1tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHhh1tag"+massPoint}, 0, 0, 1000, 1);
#else
  normFact( "Norm_ggH", {"ggHlh0tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHlh0tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHlh1tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHlh1tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHhh0tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHhh0tag"+massPoint}, 1, 0, 1000, 1);
  normFact( "Norm_ggH", {"ggHhh1tag"+massPoint}, 0, 0, 1000, 1);
  normFact( "Norm_bbH", {"bbHhh1tag"+massPoint}, 1, 0, 1000, 1);
#endif  

  /// Signal acceptance   
  double vmass = atof(massPoint.Data());
  float x[10] = {200,400,600,800,1000,1200,1400,1750,2000,2500};

  /// bbHlh1tag
  float AU_bbHlh1tag[10] = {3.62,2.6,1.74,1.88,3.17,2.37,2.82,1.55,2.29,2.13};
  TGraph g_bbHlh1tag(10, x, AU_bbHlh1tag);
  normSys(TString("ATLAS_AU_bbH")+massPoint, g_bbHlh1tag.Eval(vmass)/100, {"bbHlh1tag"+massPoint});
  /// bbHlh0tag
  float AU_bbHlh0tag[10] = {3.59,2.65,3.23,3.04,2.66,3.19,2.56,2.92,2.88,3.02};
  TGraph g_bbHlh0tag(10, x, AU_bbHlh0tag);
  normSys(TString("ATLAS_AU_bbH")+massPoint, g_bbHlh0tag.Eval(vmass)/100, {"bbHlh0tag"+massPoint});
  /// bbHhh1tag
  float AU_bbHhh1tag[10] = {7.39,2.45,2.46,2.33,2.24,2.3,1.82,1.64,1.97,1.97};
  TGraph g_bbHhh1tag(10, x, AU_bbHhh1tag);
  normSys(TString("ATLAS_AU_bbH")+massPoint, g_bbHhh1tag.Eval(vmass)/100, {"bbHhh1tag"+massPoint});
  /// bbHhh0tag
  float AU_bbHhh0tag[10] = {6.42,2.07,2.97,2.19,1.65,2.87,2.07,2.63,2.45,2.18};
  TGraph g_bbHhh0tag(10, x, AU_bbHhh0tag);
  normSys(TString("ATLAS_AU_bbH")+massPoint, g_bbHhh0tag.Eval(vmass)/100, {"bbHhh0tag"+massPoint});

  /// ggHlh1tag
  float AU_ggHlh1tag[10] = {12.68,9.6,8.02,9.6,8.78,8.0,9.44,6.89,10.09,8.3};
  TGraph g_ggHlh1tag(10, x, AU_ggHlh1tag);
  normSys(TString("ATLAS_AU_ggH")+massPoint, g_ggHlh1tag.Eval(vmass)/100, {"ggHlh1tag"+massPoint});
  /// ggHlh0tag
  float AU_ggHlh0tag[10] = {2.98,2.5,2.4,2.15,1.94,1.87,2.28,1.94,1.66,1.42};
  TGraph g_ggHlh0tag(10, x, AU_ggHlh0tag);
  normSys(TString("ATLAS_AU_ggH")+massPoint, g_ggHlh0tag.Eval(vmass)/100, {"ggHlh0tag"+massPoint});
  /// ggHhh1tag
  float AU_ggHhh1tag[10] = {23.07,11.68,7.29,8.31,7.3,7.41,6.39,6.42,8.08,6.74};
  TGraph g_ggHhh1tag(10, x, AU_ggHhh1tag);
  normSys(TString("ATLAS_AU_ggH")+massPoint, g_ggHhh1tag.Eval(vmass)/100, {"ggHhh1tag"+massPoint});
  /// ggHhh0tag
  float AU_ggHhh0tag[10] = {4.68,1.94,2.01,2.07,1.99,1.55,1.39,1.25,1.2,1.2};
  TGraph g_ggHhh0tag(10, x, AU_ggHhh0tag);
  normSys(TString("ATLAS_AU_ggH")+massPoint, g_ggHhh0tag.Eval(vmass)/100, {"ggHhh0tag"+massPoint});

  /// cross sections
  normSys("ATLAS_xsec_Top",     0.06, {  "Top"  } );
  normSys("ATLAS_xsec_Diboson", 0.1, {  "Diboson" } );
  /// lumi
  normSys("ATLAS_LUMI", 0.032, { { "DYZ", "Diboson", "Top", "ZplusJets", "Wtaunu",  "Ztautau","Others", 
                                    "bbHlh1tag"+massPoint, "ggHlh1tag"+massPoint, "bbHhh1tag"+massPoint, "ggHhh1tag"+massPoint,
                                    "bbHlh0tag"+massPoint, "ggHlh0tag"+massPoint, "bbHhh0tag"+massPoint, "ggHhh0tag"+massPoint } } );

  // print all the systematics
  std::cout << "Print all the user systematics:" << std::endl;
  for (auto systs: m_userSysts) {
    systs.first.print();
    for (auto sample: systs.second.sampleNames)
      std::cout << "  - sample : " << sample << std::endl;
  }
}


void SystematicListsBuilder_Htautau::listAllHistoSystematics() {
  /// configs: how to treat hist systs
  using T = SysConfig::Treat;
  using S = SysConfig::Smooth;
  using Sym = SysConfig::Symmetrise;
  SysConfig normOneSideSymConfig { T::norm, S::noSmooth, Sym::symmetriseOneSided };
  SysConfig noSmoothConfig { T::shape, S::noSmooth };
  SysConfig smoothConfig { T::shape, S::smoothRebinMonotonic };
  SysConfig noSmoothOneSidedSymConfig { T::shape, S::noSmooth, Sym::symmetriseOneSided };
  SysConfig smoothOneSideSymConfig { T::shape, S::smoothRebinMonotonic, Sym::symmetriseOneSided };
  SysConfig skipConfig { T::skip, S::noSmooth };

  /// ***********************************************************
  /// lephad, hadhad common systematics
  /// ***********************************************************
  /// 12(kfactor) + 4(ttbar) + 17(jet) + 14(btagging) + 7(Tau) + 3(MET) + 1(PU) + 2(old ttbar, only in lephad)= 58 (60)
  /// kfactor: 12
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_ALPHAS_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_BEAM_ENERGY_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_CHOICE_HERAPDF20_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_CHOICE_NNPDF30_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_CHOICE_epWZ16_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_PDF_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_PDF_EW_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_PDF_epWZ16_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_PI_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_REDCHOICE_NNPDF30_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_SCALE_W_Corr", smoothConfig });
  m_histoSysts.insert({ "ATLAS_LPX_KFACTOR_SCALE_Z_Corr", smoothConfig });
  /// ttbar: 4
#if 1
  m_histoSysts.insert({ "ATLAS_TTBAR_Radiation", skipConfig });
  m_histoSysts.insert({ "ATLAS_TTBAR_ShowerUE", skipConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_ISR", noSmoothConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_PS", noSmoothOneSidedSymConfig });

  SysConfig topDecorrConfig = noSmoothConfig;
  topDecorrConfig.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                TString regionName = pset(Property::descr);
                                if (regionName.Contains("Tcr")) return "_TCR";
                                else return "";}
    );
  SysConfig topDecorrConfigOneSide = noSmoothOneSidedSymConfig;
  topDecorrConfigOneSide.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                TString regionName = pset(Property::descr);
                                if (regionName.Contains("Tcr")) return "_TCR";
                                else return "";}
    );
  m_histoSysts.insert({ "ATLAS_TTBar_FSR", topDecorrConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_ME", topDecorrConfigOneSide });

#else
  m_histoSysts.insert({ "ATLAS_TTBAR_Radiation", smoothConfig });
  m_histoSysts.insert({ "ATLAS_TTBAR_ShowerUE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_FSR", skipConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_ISR", skipConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_ME",  skipConfig });
  m_histoSysts.insert({ "ATLAS_TTBar_PS",  skipConfig });
#endif
  /// JET: 17
  m_histoSysts.insert({ "ATLAS_JET_EtaIntercalibration_NonClosure_highE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_EtaIntercalibration_NonClosure_negEta", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_EtaIntercalibration_NonClosure_posEta", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_Flavor_Response", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_GroupedNP_1", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_GroupedNP_2", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_GroupedNP_3", smoothConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_DataVsMC_MC16", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_1", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_2", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_3", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_4", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_5", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_6", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_JER_EffectiveNP_7restTerm", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_JET_TILECORR_Uncertainty", smoothConfig });
  m_histoSysts.insert({ "ATLAS_jet_jvteff", smoothConfig });
  /// b-JET: 14
  m_histoSysts.insert({ "ATLAS_btag_b_0", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_b_1", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_b_2", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_c_0", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_c_1", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_c_2", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_c_3", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_extrapolation", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_extrapolation_from_charm", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_light_0", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_light_1", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_light_2", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_light_3", smoothConfig });
  m_histoSysts.insert({ "ATLAS_btag_light_4", smoothConfig });
  /// TAU: 7
  m_histoSysts.insert({ "ATLAS_tau_eff_reco_highpt", smoothConfig });
  m_histoSysts.insert({ "ATLAS_tau_eff_reco_total", smoothConfig });
  m_histoSysts.insert({ "ATLAS_TAUS_TRUEHADTAU_SME_TES_DETECTOR", smoothConfig });
  m_histoSysts.insert({ "ATLAS_TAUS_TRUEHADTAU_SME_TES_INSITU", smoothConfig });
  m_histoSysts.insert({ "ATLAS_TAUS_TRUEHADTAU_SME_TES_MODEL", smoothConfig });
  m_histoSysts.insert({ "ATLAS_tau_eff_jetid_highpt", smoothConfig});
  m_histoSysts.insert({ "ATLAS_tau_eff_jetid_total", smoothConfig});
  // MET: 3
  m_histoSysts.insert({ "ATLAS_MET_SoftTrk_ResoPara", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_MET_SoftTrk_ResoPerp", smoothOneSideSymConfig });
  m_histoSysts.insert({ "ATLAS_MET_SoftTrk_Scale", smoothConfig });
  // pileup: 1
  m_histoSysts.insert({ "ATLAS_pu_prw", smoothConfig });


  /// ***********************************************************
  /// lephad specific systematics 60(common)+41= 101 (2 not used)
  /// ***********************************************************
  // electron trigger, rec, id, resolution, scale: 12
  m_histoSysts.insert({ "ATLAS_EG_RESOLUTION_ALL", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_AF2", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_ALL", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_ALLCORR", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_E4SCINTILLATOR", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_LARCALIB_EXTRA2015PRE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_LARTEMPERATURE_EXTRA2015PRE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_EG_SCALE_LARTEMPERATURE_EXTRA2016PRE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_el_eff_id", smoothConfig });
  m_histoSysts.insert({ "ATLAS_el_eff_iso", smoothConfig });
  m_histoSysts.insert({ "ATLAS_el_eff_reco", smoothConfig });
  m_histoSysts.insert({ "ATLAS_el_eff_trigger", smoothConfig });
  //  muon trigger, rec, id, resolution, scale: 15
  m_histoSysts.insert({ "ATLAS_MUON_ID", smoothConfig });
  m_histoSysts.insert({ "ATLAS_MUON_MS", smoothConfig });
  m_histoSysts.insert({ "ATLAS_MUON_SAGITTA_RESBIAS", smoothConfig });
  m_histoSysts.insert({ "ATLAS_MUON_SAGITTA_RHO", skipConfig });
  m_histoSysts.insert({ "ATLAS_MUON_SCALE", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_isostat", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_isosys", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_stat", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_statlowpt", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_sys", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_syslowpt", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_trigstat", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_trigsys", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_ttvastat", smoothConfig });
  m_histoSysts.insert({ "ATLAS_mu_eff_ttvasys", smoothConfig });
  // FF: 22
  m_histoSysts.insert({ "ATLAS_FakeFactor_LepElBtag", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_LepElBveto", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_LepMuBtag", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_LepMuBveto", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsBtag1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsBtag3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsBveto1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsBveto3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_ExtraSysBveto1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_ExtraSysBveto3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_ExtraSysBtag1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_ExtraSysBtag3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_QCDReweight_MuHadBtag", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_QCDReweight_MuHadBveto", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_ElHadBveto1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_ElHadBveto3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_MuHadBveto1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_MuHadBveto3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_ElHadBtag1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_ElHadBtag3p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_MuHadBtag1p", smoothConfig });
  m_histoSysts.insert({ "ATLAS_FakeFactor_WjetsReweight_MuHadBtag3p", smoothConfig });
  // tau: 2
  m_histoSysts.insert({ "ATLAS_tau_eff_eleolr_trueelectron", smoothConfig });
  m_histoSysts.insert({ "ATLAS_tau_eff_eleolr_truehadtau", smoothConfig });

  /// ***********************************************************
  /// hadhad specific systematics 58(common) + 56 = 114 (2 are not used)
  /// ***********************************************************
  // tau trigger: 12
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018", smoothConfig });
  /// FFs: 34 ( 2 not used)
  m_histoSysts.insert({ "HHFAKERATE_REDONE", smoothConfig});
  m_histoSysts.insert({ "QCDFF_BINC_BVETO_sys", noSmoothConfig });
  m_histoSysts.insert({ "QCDFF_BINC_BTAG_sys", noSmoothConfig });
  m_histoSysts.insert({ "QCDFF_BINC_syst", noSmoothConfig });
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin1", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin2", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin3", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin4", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin5", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin6", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin7", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin8", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_data_bin9", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin1", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin2", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin3", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin4", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin5", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin6", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin7", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin8", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_1p_OS_BINC_stat_mc_bin9", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_data_bin1", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_data_bin2", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_data_bin3", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_data_bin4", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_data_bin5", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_mc_bin1", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_mc_bin2", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_mc_bin3", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_mc_bin4", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_3p_OS_BINC_stat_mc_bin5", noSmoothConfig});
  m_histoSysts.insert({ "QCDFF_BINC_stat_data", skipConfig });
  m_histoSysts.insert({ "QCDFF_BINC_stat_mc", skipConfig });
  /// tau: 10
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__LTAU_MED", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__SLTAU_LOOSE", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__LTAU_MED", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__SLTAU_LOOSE", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__SLTAU_LOOSE", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_SYST__SLTAU_LOOSE", smoothConfig });
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_SME_TES_AFII", smoothConfig});
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_RECO_AFII", smoothConfig});
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_AFII__LTAU_MED", smoothConfig});
  m_histoSysts.insert({ "TAUS_TRUEHADTAU_EFF_JETID_AFII__SLTAU_LOOSE", smoothConfig});

  m_histoSysts.insert({ "MAT_TRANS_SYS", noSmoothConfig});

  ///***********************************************************************************
  /// Print all the hist systematics
  ///***********************************************************************************
  std::cout << "Print all the histo systematics: " << m_histoSysts.size() << std::endl;
  for (auto histSyst : m_histoSysts)
      std::cout << histSyst.first << std::endl;
}
