#include "binning_htt.hpp"

#include <iostream>
#include <cmath>
#include <map>
#include <memory>

#include <TH1.h>
#include <TString.h>

#include "WSMaker/binning.hpp"
#include "WSMaker/configuration.hpp"
#include "WSMaker/category.hpp"
#include "WSMaker/properties.hpp"

#include "TransformTool/HistoTransform.h"

class Sample;

BinningTool_Htautau::BinningTool_Htautau(const Configuration& conf) :
  BinningTool(conf)
{

}

void BinningTool_Htautau::createInstance(const Configuration& conf) {
  if( !the_instance ) {
    std::cout << "INFO:    BinningTool_Htautau::createInstance() BinningTool pointer is NULL." << std::endl;
    std::cout << "         Will instanciate the BinningTool service first." << std::endl;
    the_instance.reset(new BinningTool_Htautau(conf));
  }
  else {
    std::cout << "WARNING: BinningTool_Htautau::createInstance() BinningTool pointer already exists." << std::endl;
    std::cout << "         Don't do anything !" << std::endl;
  }
}


std::vector<int> BinningTool_Htautau::getCategoryBinning(const Category& c) {

  /// result
  std::vector<double> res;
  res.clear();

  /// hadhad
  if (c[Property::nLep] == 0 ) { 
    if (c[Property::nTag] == 0) {
      /// hadhad bveto
      res = {150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1150, 5000};
    }
    else if (c[Property::nTag] == 1) {
      /// hadhad btag
      res = {150, 200, 250, 300, 350, 400, 450, 500, 550, 650, 5000};
    }
  }
  /// lephad
  if (c[Property::nLep] == 1 ){ 
    if (c[Property::nTag] == 0 && c(Property::descr) == "MuHad") {
      /// muhad bveto 
      //res = {50,70,80,90,100,120,140,160,180,200,220,240,260,280,300,500,5000};
      res = {50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,5000}; 
    }
    else if (c[Property::nTag] == 0 && c(Property::descr) == "ElHad") {
      /// ehad bveto
      //res = {50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,5000};
      res = {50, 70, 80, 90, 100,120,140,160,180,200,220,240,260,280,300,320,340,360,400,500,600,700,800,5000}; 
    }
    else if (c[Property::nTag] == 1 && c(Property::descr) == "MuHad") {
      /// muhad btag
      //res = {50, 100, 150, 200, 250, 300, 400, 5000};
      res = {50, 100, 150, 200, 250, 300, 350, 400, 500, 5000};
    }
    else if (c[Property::nTag] == 1 && c(Property::descr) == "ElHad") {
      /// ehad btag
      // res = {50, 100, 150, 200, 250, 300, 350, 400, 500, 5000};
      res = {50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 5000};
    }
    else if (c[Property::nTag] == 1 && c(Property::descr) == "MuHadTcr") {
      /// muhad btag tcr
      //res = {100, 150, 200, 250, 300, 350, 400, 450, 500, 5000};
      res = {100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 5000};
    }
    else if (c[Property::nTag] == 1 && c(Property::descr) == "ElHadTcr") {
      /// ehad btag tcr
      //res = {100, 150, 200, 250, 300, 350, 400, 500, 5000};
      res = {100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 800, 5000};
    }
  }
  /// print some info
  std::cout << "INFO:    BinningTool_Htautau::Rebin Category " << c.name() << std::endl;
  for (auto bin : res)
    std::cout << bin << "  ";
  std::cout << std::endl;


  if ( 0 != res.size())
    return makeForcedBinning(c, res);
  else {
    std::vector<int> hack;
    hack.clear();
    return hack;
  } 
}

