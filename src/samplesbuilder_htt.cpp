#include "samplesbuilder_htt.hpp"

#include <set>
#include <unordered_map>
#include <utility>
#include <iostream>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/sample.hpp"

void SamplesBuilder_Htautau::declareSamples() {

  TString massPoint = m_config.getValue("MassPoint", "125");
  // data or pseudo-data should always be there
  addData();

  //*********************************************************
  // signal
  //*********************************************************
  // lephad
  addSample(Sample("ggHlh1tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("ggHlh0tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("bbHlh1tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("bbHlh0tag"+massPoint,  SType::Sig, kRed));
  // hadhad
  addSample(Sample("ggHhh1tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("ggHhh0tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("bbHhh1tag"+massPoint,  SType::Sig, kRed));
  addSample(Sample("bbHhh0tag"+massPoint,  SType::Sig, kRed));

  //*********************************************************
  // background
  //*********************************************************
  //lephad
  addBkgs( { {"DYZ", kCyan+3} } );
  addBkgs( { {"Diboson", kGreen} } );

  addBkgs( { {"QCDFakes", kYellow} } );
  addBkgs( { {"WJETSFakes", kYellow} } );
  addBkgs( { {"Fakes", kYellow} } ); 

  addBkgs( { {"Top", kOrange+3} } );  // common sample
  addBkgs( { {"ZplusJets", kBlue} } );

  //hadhad
  addBkgs( { {"Others",   kGreen+4} } );  
  addBkgs( { {"Multijet", kYellow} } );

  addBkgs( { {"Ztautau", kBlue-3 } } );  
  addBkgs( { {"Wtaunu", kOrange+3 } } );   
}

void SamplesBuilder_Htautau::declareKeywords() {
  // keywords

  // Print 
}

void SamplesBuilder_Htautau::declareSamplesToMerge() {
  TString massPoint = m_config.getValue("MassPoint", "125");
  // serves as renaming !!! since we only have one histogram
  declareMerging(Sample("bbH" +massPoint,  SType::Sig, kRed  ), {"bbHlh1tag"+massPoint, "bbHlh0tag"+massPoint,"bbHhh1tag"+massPoint, "bbHhh0tag"+massPoint} );
  declareMerging(Sample("ggH" +massPoint,  SType::Sig, kRed  ), {"ggHlh1tag"+massPoint, "ggHlh0tag"+massPoint,"ggHhh1tag"+massPoint, "ggHhh0tag"+massPoint} );  
}

